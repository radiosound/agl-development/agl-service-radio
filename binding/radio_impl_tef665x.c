/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
	TODO:
		at this point:
			- complete the rest of verbs.
			- find a way to tell the service which i2c chanel is used.
			- separate the functions of driver from the verbs by creating new c file.
			- find a way of monitoring the quality of tuning and correct it time by time.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <glib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <stdarg.h>
#include <error.h>
#include <gst/gst.h>

//#include <json-c/json.h>
//#include <gst/gst.h>

#include <afb/afb-binding.h>

#include "radio_impl.h"
#include "tef665x.h"


#define I2C_ADDRESS 0x64
#define I2C_DEV "/dev/i2c-3"
#define VERSION "0.1"

#define TEF665x_CMD_LEN_MAX	20
#define SET_SUCCESS 1
#define TEF665X_SPLIT_SIZE		24

#define TEF665x_REF_CLK		9216000	//reference clock frequency
#define TEF665x_IS_CRYSTAL_CLK	0	//crstal
#define TEF665x_IS_EXT_CLK	1	//external clock input

#define High_16bto8b(a)	((u8)((a) >> 8))
#define Low_16bto8b(a) 	((u8)(a))
#define Convert8bto16b(a)	((ushort)(((ushort)(*(a))) << 8 |((ushort)(*(a+1)))))

#define GST_PIPELINE_LEN    256

const u8 tef665x_patch_cmdTab1[] = {3,	0x1c,0x00,0x00};
const u8 tef665x_patch_cmdTab2[] = {3,	0x1c,0x00,0x74};
const u8 tef665x_patch_cmdTab3[] = {3,	0x1c,0x00,0x75};

typedef struct {
	char *name;
	uint32_t min;
	uint32_t max;
	uint32_t step;
} band_plan_t;

static band_plan_t known_fm_band_plans[5] = {
	{ .name = "US", .min = 87900000, .max = 107900000, .step = 200000 },
	{ .name = "JP", .min = 76000000, .max = 95000000, .step = 100000 },
	{ .name = "EU", .min = 87500000, .max = 108000000, .step = 50000 },
	{ .name = "ITU-1", .min = 87500000, .max = 108000000, .step = 50000 },
	{ .name = "ITU-2", .min = 87900000, .max = 107900000, .step = 50000 }
};
static band_plan_t known_am_band_plans[1] = {
	{ .name = "W-ASIA", .min = 522000, .max = 1620000, .step = 5000 }
};

static unsigned int fm_bandplan = 2;
static unsigned int am_bandplan = 0;
static bool corking = false;
static bool present = false;
static bool scanning = false;

// stream state
static GstElement *pipeline;
static bool running;

#define DEBUG 0

#if DEBUG == 1
#define _debug(x, y) printf("function: %s,  %s : %d\n", __FUNCTION__, #x, y)
#else
#define _debug(x, y)
#endif

static uint file_desc;

static radio_band_t current_band;
static uint32_t current_am_frequency;
static uint32_t current_fm_frequency;

static gboolean handle_message(GstBus *bus, GstMessage *msg, __attribute__((unused)) void *ptr)
{
	GstState state;

	if (GST_MESSAGE_TYPE(msg) == GST_MESSAGE_REQUEST_STATE) {

		gst_message_parse_request_state(msg, &state);

		if (state == GST_STATE_PAUSED)
			corking = true;
		else if (state == GST_STATE_PLAYING)
			corking = false;

	}

	return TRUE;
}

static int tef665x_set_cmd(int i2c_file_desc, TEF665x_MODULE module, u8 cmd, int len, ...)
{
	int i, ret;
	u8 buf[TEF665x_CMD_LEN_MAX];
	ushort temp;
    va_list vArgs;

    va_start(vArgs, len);

	buf[0] = module;	//module,	FM/AM/APP
	buf[1] = cmd;		//cmd,		1,2,10,...
	buf[2] = 0x01;	    //index, 	always 1

	for(i = 3; i < len; i++)
	{
		temp = va_arg(vArgs,int);

		buf[i++] = High_16bto8b(temp);
		buf[i] = Low_16bto8b(temp);
	}

	va_end(vArgs);

	ret = write(i2c_file_desc, buf, len);

	temp = (ret == len) ? 1 : 0;
	_debug("return value", temp);
	return temp;
}

static int tef665x_get_cmd(int i2c_file_desc, TEF665x_MODULE module, u8 cmd, u8 *receive, int len)
{
	u8 temp;
	u8 buf[3];
	int ret;

	buf[0]= module;		//module,	FM/AM/APP
	buf[1]= cmd;		//cmd,		1,2,10,...
	buf[2]= 1;	        //index, 	always 1

	write(i2c_file_desc, buf, 3);

	ret = read(i2c_file_desc, receive, len);
	temp = (ret == len) ? 1 : 0;
	_debug("return value", temp);
	return temp;
}

/*
module 64 APPL
cmd 128 Get_Operation_Status | status
index
1 status
	Device operation status
	0 = boot state; no command support
	1 = idle state
	2 = active state; radio standby
	3 = active state; FM
	4 = active state; AM
*/
static int appl_get_operation_status(int i2c_file_desc ,u8 *status)
{
   	u8 buf[2];
	int ret;

    ret = tef665x_get_cmd(i2c_file_desc, TEF665X_MODULE_APPL,
			TEF665X_Cmd_Get_Operation_Status,
			buf, sizeof(buf));

	if(ret == SET_SUCCESS)
	{
		*status = Convert8bto16b(buf);
		_debug("return value", 1);
		return 1;
	}
	_debug("return value", 0);
	return 0;
}

static int get_operation_status(int i2c_file_desc, TEF665x_STATE *status)
{
	TEF665x_STATE data;
	int ret;
	if(SET_SUCCESS ==(ret = appl_get_operation_status(i2c_file_desc, &data)))
	{
		//printk( "appl_get_operation_status1 data= %d \n",data);
		_debug("got status", ret);
		switch(data)
		{
			case 0:
				_debug("status: boot", ret);
				*status = eDevTEF665x_Boot_state;
				break;
			case 1:
				_debug("status: idle", ret);
				*status = eDevTEF665x_Idle_state;
				break;
			default:
				_debug("status: active", ret);
				*status = eDevTEF665x_Active_state;
				break;
		}
	}
	return ret;
}

static int tef665x_power_on(int i2c_file_desc)
{
	int ret;
	TEF665x_STATE status;
	usleep(5000);
	if(SET_SUCCESS == (ret = get_operation_status(i2c_file_desc, &status)))   //[ w 40 80 01 [ r 0000 ]
	{
		_debug("Powered ON", ret);
	}
	else
	{
		_debug("Powered ON FAILED!", ret);
	}

	return ret;
}

static int tef665x_writeTab(int i2c_file_desc,const u8 *tab)
{
	int ret;
	ret = write(i2c_file_desc, tab + 1, tab[0]);
	return (ret != tab[0]) ? 0 : 1;
}

static int tef665x_patch_load(int i2c_file_desc, const u8 *bytes, ushort size)
{
	u8 buf[25]; //the size which we break the data into, is 24 bytes.
	int ret, i;

    ushort num = size / 24;
	ushort rem = size % 24;

    buf[0] = 0x1b;

    usleep(10000);

    for(i = 0; i < num; i++)
    {
		memcpy(buf + 1, bytes + (24 * i), 24);

		ret = write(i2c_file_desc, buf, 25);

		if(ret != 25)
		{
			_debug("FAILED, send patch error! in pack no", i);
			return false;
		}
		usleep(50);
	}

    memcpy(buf + 1, bytes + (num * 24), rem);

    ret = write(i2c_file_desc, buf, rem);
		if(ret != rem)
		{
			_debug("FAILED, send patch error at the end!", 0);
			return false;
		}
	usleep(50);

	_debug("return value", 1);
	return true;
}

static int tef665x_patch_init(int i2c_file_desc)
{
	int ret = 0;
	ret = tef665x_writeTab(i2c_file_desc, tef665x_patch_cmdTab1);  //[ w 1C 0000 ]
	if(!ret)
	{
		_debug("1- tab1 load FAILED", ret);
		return ret;
	}

	ret = tef665x_writeTab(i2c_file_desc, tef665x_patch_cmdTab2);  //[ w 1C 0074 ]
	if(!ret)
	{
		_debug("2- tab2 load FAILED", ret);
		return ret;
	}

	ret = tef665x_patch_load(i2c_file_desc, pPatchBytes, patchSize); //table1
	if(!ret)
	{
		_debug("3- pPatchBytes load FAILED", ret);
		return ret;
	}

	ret = tef665x_writeTab(i2c_file_desc, tef665x_patch_cmdTab1); //[ w 1C 0000 ]
	if(!ret)
	{
		_debug("4- tab1 load FAILED", ret);
		return ret;
	}

	ret = tef665x_writeTab(i2c_file_desc, tef665x_patch_cmdTab3); //[ w 1C 0075 ]
	if(!ret)
	{
		_debug("5- tab3 load FAILED", ret);
		return ret;
	}

	ret = tef665x_patch_load(i2c_file_desc, pLutBytes, lutSize); //table2
	if(!ret)
	{
		_debug("6- pLutBytes load FAILED", ret);
		return ret;
	}

	ret = tef665x_writeTab(i2c_file_desc, tef665x_patch_cmdTab1); //[ w 1C 0000 ]
	if(!ret)
	{
		_debug("7- tab1 load FAILED", ret);
		return ret;
	}
	_debug("patch loaded", ret);
	return ret;
}

//Command start will bring the device into? idle state�: [ w 14 0001 ]
static int tef665x_start_cmd(int i2c_file_desc)
{

	int ret;
	unsigned char  buf[3];

	buf[0] = 0x14;
	buf[1] = 0;
	buf[2] = 1;

	ret = write(i2c_file_desc, buf, 3);

	if (ret != 3)
	{
		_debug("start cmd FAILED", 0);
		return 0;
	}
	_debug("return true", 1);
	return 1;
}

static int tef665x_boot_state(int i2c_file_desc)
{
	int ret=0;
	if(1 == tef665x_patch_init(i2c_file_desc))
	{
		_debug("return true", 1);
	}
	else
	{
		_debug("return value", 0);
		return 0;
	}

	usleep(50000);

	if(1 == tef665x_start_cmd(i2c_file_desc))
	{
		_debug("'start cmd'return true", 1);
	}
	else
	{
		_debug("return value", 0);
		return 0;
	}

	usleep(50000);

	return ret;
}

/*
module 64 APPL
cmd 4 Set_ReferenceClock frequency

index
1 frequency_high
	[ 15:0 ]
	MSB part of the reference clock frequency
	[ 31:16 ]
2 frequency_low
	[ 15:0 ]
	LSB part of the reference clock frequency
	[ 15:0 ]
	frequency [*1 Hz] (default = 9216000)
3 type
	[ 15:0 ]
	clock type
	0 = crystal oscillator operation (default)
	1 = external clock input operation
*/
static int tef665x_appl_set_referenceClock(uint i2c_file_desc, ushort frequency_high, ushort frequency_low, ushort type)
{
	return tef665x_set_cmd(i2c_file_desc, TEF665X_MODULE_APPL,
			TEF665X_Cmd_Set_ReferenceClock,
			11,
			frequency_high, frequency_low, type);
}

static int appl_set_referenceClock(uint i2c_file_desc, uint frequency, bool is_ext_clk)  //0x3d 0x900
{
	return tef665x_appl_set_referenceClock(i2c_file_desc,(ushort)(frequency >> 16), (ushort)frequency, is_ext_clk);
}

/*
module 64 APPL
cmd 5 Activate mode

index
1 mode
	[ 15:0 ]
	1 = goto �active� state with operation mode of �radio standby�
*/
static int tef665x_appl_activate(uint i2c_file_desc ,ushort mode)
{
	return tef665x_set_cmd(i2c_file_desc, TEF665X_MODULE_APPL,
			TEF665X_Cmd_Activate,
			5,
			mode);
}

static int appl_activate(uint i2c_file_desc)
{
	return tef665x_appl_activate(i2c_file_desc, 1);
}
/*
module 48 AUDIO
cmd 22 set_dig_io signal, format, operation, samplerate

index
1 signal
[ 15:0 ]
	digital audio input / output
	32 = I²S digital audio IIS_SD_0 (input)
	33 = I²S digital audio IIS_SD_1 (output)
(2) mode
	0 = off (default)
	1 = input (only available for signal = 32)
	2 = output (only available for signal = 33)
(3) format
	[ 15:0 ]
	digital audio format select
	16 = I²S 16 bits (fIIS_BCK = 32 * samplerate)
	32 = I²S 32 bits (fIIS_BCK = 64 * samplerate) (default)
	272 = lsb aligned 16 bit (fIIS_BCK = 64 * samplerate)
	274 = lsb aligned 18 bit (fIIS_BCK = 64 * samplerate)
	276 = lsb aligned 20 bit (fIIS_BCK = 64 * samplerate)
	280 = lsb aligned 24 bit (fIIS_BCK = 64 * samplerate)
(4) operation
	[ 15:0 ]
	operation mode
	0 = slave mode; IIS_BCK and IIS_WS input defined by source (default)
	256 = master mode; IIS_BCK and IIS_WS output defined by device
(5) samplerate
	[ 15:0 ] 3200 = 32.0 kHz
	4410 = 44.1 kHz (default)
	4800 = 48.0 kHz
*/
static int tef665x_audio_set_dig_io(u8 i2c_file_desc, ushort signal, ushort mode, ushort format, ushort operation, ushort samplerate)
{
	int ret = tef665x_set_cmd(i2c_file_desc, TEF665X_MODULE_AUDIO,
				TEF665X_Cmd_Set_Dig_IO,
				15,
				signal, mode, format, operation, samplerate);
	if(ret)
	{
		_debug("Digital In/Out is set ", signal);
	}
	else
	{
		_debug("FAILED, return", 0);
		return 0;
	}
	return 1;
}

/*
module 32 / 33 FM / AM
cmd 85 Set_Specials ana_out, dig_out

index
1 signal
	[ 15:0 ]
	analog audio output
	128 = DAC L/R output
2 mode
	[ 15:0 ]
	output mode
	0 = off (power down)
	1 = output enabled (default)
*/

static int tef665x_audio_set_ana_out(uint i2c_file_desc, ushort signal,ushort mode)
{
	int ret = tef665x_set_cmd(i2c_file_desc, TEF665X_MODULE_AUDIO,
		      TEF665X_Cmd_Set_Ana_Out,
			  7,
			  signal, mode);
	if(ret)
	{
		_debug("analog output is set to ", mode);
	}
	else
	{
		_debug("FAILED, return", 0);
		return 0;
	}
	return 1;

}

/*
module 48 AUDIO
cmd 13 Set_Output_Source

index
1 signal
	[ 15:0 ]
	audio output
	33 = I2S Digital audio
	128 = DAC L/R output (default)
2 source
	[ 15:0 ]
	source
	4 = analog radio
	32 = i2s digital audio input
	224 = audio processor (default)
	240 = sin wave generator
*/
static int  tef665x_set_output_src(uint i2c_file_desc, u8 signal, u8 src)
{
	int ret = tef665x_set_cmd(i2c_file_desc, TEF665X_MODULE_AUDIO,
			  TEF665X_Cmd_Set_Output_Source,
			  7,
			  signal, src);
	if(ret)
	{
		_debug("Output is set ", signal);
	}
	else
	{
		_debug("FAILED, return", 0);
		return 0;
	}
	return 1;
}

static int tef665x_idle_state(int i2c_file_desc)
{
	TEF665x_STATE status;

	//mdelay(50);

	if(SET_SUCCESS == get_operation_status(i2c_file_desc, &status))
	{
		_debug("got operation status", 1);
 	    if(status != eDevTEF665x_Boot_state)
		{
			_debug("not in boot status", 1);

			if(SET_SUCCESS == appl_set_referenceClock(i2c_file_desc, TEF665x_REF_CLK, TEF665x_IS_CRYSTAL_CLK)) //TEF665x_IS_EXT_CLK
			{
				_debug("set the clock", TEF665x_REF_CLK);
				if(SET_SUCCESS == appl_activate(i2c_file_desc))// APPL_Activate mode = 1.[ w 40 05 01 0001 ]
				{
					//usleep(100000); //Wait 100 ms
					_debug("activate succeed", 1);
					return 1;
				}
				else
				{
					_debug("activate FAILED", 1);
				}
			}
			else
			{
				_debug("set the clock FAILED", TEF665x_REF_CLK);
			}

		}
		else
		{
			_debug("did not get operation status", 0);
		}

	}
	_debug("return value", 0);
	return 0;
}

static int tef665x_para_load(uint i2c_file_desc)
{
	int i;
	int r;
	const u8 *p = init_para;

	for(i = 0; i < sizeof(init_para); i += (p[i]+1))
	{
		if(SET_SUCCESS != (r = tef665x_writeTab(i2c_file_desc, p + i)))
		{
			break;
		}
	}

	_debug("return value", r);
	return r;
}

/*
module 32 / 33 FM / AM
cmd 1 Tune_To mode, frequency

index
1 mode
	[ 15:0 ]
	tuning actions
	0 = no action (radio mode does not change as function of module band)
	1 = Preset Tune to new program with short mute time
	2 = Search Tune to new program and stay muted
	FM 3 = AF-Update Tune to alternative frequency, store quality
	and tune back with inaudible mute
	4 = Jump Tune to alternative frequency with short
	inaudible mute
	5 = Check Tune to alternative frequency and stay
	muted
	AM 3 � 5 = reserved
	6 = reserved
	7 = End Release the mute of a Search or Check action
	(frequency is not required and ignored)
2 frequency
[ 15:0 ]
	tuning frequency
	FM 6500 � 10800 65.00 � 108.00 MHz / 10 kHz step size
	AM LW 144 � 288 144 � 288 kHz / 1 kHz step size
	MW 522 � 1710 522 � 1710 kHz / 1 kHz step size
	SW 2300 � 27000 2.3 � 27 MHz / 1 kHz step size
*/
static int tef665x_radio_tune_to (uint i2c_file_desc, bool fm, ushort mode,ushort frequency )
{
	return tef665x_set_cmd(i2c_file_desc, fm ? TEF665X_MODULE_FM: TEF665X_MODULE_AM,
			TEF665X_Cmd_Tune_To,
			( mode <= 5 ) ? 7 : 5,
			mode, frequency);
}

static int FM_tune_to(uint i2c_file_desc, AR_TuningAction_t mode, ushort frequency)
{
	int ret = tef665x_radio_tune_to(i2c_file_desc, 1, (ushort)mode, frequency);
	_debug("return value", ret);
	return ret;
}

static int AM_tune_to(uint i2c_file_desc, AR_TuningAction_t mode,ushort frequency)
{
	int ret = tef665x_radio_tune_to(i2c_file_desc, 0, (ushort)mode, frequency);
	_debug("return value", ret);
	return ret;
}

/*
module 48 AUDIO
cmd 11 Set_Mute mode

index
1 mode
	[ 15:0 ]
	audio mute
	0 = mute disabled
	1 = mute active (default)
*/
int tef665x_audio_set_mute(uint i2c_file_desc, ushort mode)
{
	int ret = tef665x_set_cmd(i2c_file_desc, TEF665X_MODULE_AUDIO,
			  TEF665X_Cmd_Set_Mute,
			  5,
			  mode);
	if(ret)
	{
		_debug("mute state changed , mode", mode);
	}
	else
	{
		_debug("FAILED, return", 0);
		return 0;
	}
	return 1;
}

/*
module 48 AUDIO
cmd 10 Set_Volume volume

index
1 volume
	[ 15:0 ] (signed)
	audio volume
	-599 � +240 = -60 � +24 dB volume
	0 = 0 dB (default)f665x_patch_init function:  "3"t,int16_t volume)
*/
static int tef665x_audio_set_volume(uint i2c_file_desc, ushort volume)
{
	return tef665x_set_cmd(i2c_file_desc, TEF665X_MODULE_AUDIO,
			TEF665X_Cmd_Set_Volume,
			5,
			volume*10);
}
/*
module 64 APPL
cmd 130 Get_Identification
index
1 device
2 hw_version
3 sw_version
*/
int appl_get_identification(int i2c_file_desc)
{
    u8 buf[6];
    int ret;

    ret = tef665x_get_cmd(i2c_file_desc, TEF665X_MODULE_APPL,
            TEF665X_Cmd_Get_Identification,
			buf, sizeof(buf));
// should be completed for further use
// extracting chip versions ...
    if(ret == SET_SUCCESS)
	{
		for(int i = 0; i<6;i++)
			printf("buf[%i] = %x\n", i, buf[i]);
		return 1;
	}
	_debug("return value", 0);
	return 0;
}


//mute=1, unmute=0
int audio_set_mute(uint i2c_file_desc, bool mute)
{
	return tef665x_audio_set_mute(i2c_file_desc, mute);//AUDIO_Set_Mute mode = 0 : disable mute
}

//-60 � +24 dB volume
int audio_set_volume(uint i2c_file_desc, int vol)
{
	return tef665x_audio_set_volume(i2c_file_desc, (ushort)vol);
}

/*
module 64 APPL
cmd 1 Set_OperationMode mode

index
1 mode
	[ 15:0 ]
	device operation mode
	0 = normal operation
	1 = radio standby mode (low-power mode without radio functionality)
	(default)
*/

static int tef665x_audio_set_operationMode(uint i2c_file_desc, ushort mode)
{
	_debug("normal: 0   standby: 1   requested", 1);
	int ret = tef665x_set_cmd(i2c_file_desc, TEF665X_MODULE_APPL,
			  TEF665X_Cmd_Set_OperationMode,
			  5,
			  mode);
	if(ret)
	{
		_debug("was able to set the mode", ret);
	}
	else
	{
		_debug("FAILED, return", 0);
		return 0;
	}
	return 1;
}



//TRUE = ON;
//FALSE = OFF
static void radio_powerSwitch(uint i2c_file_desc, bool OnOff)
{
	tef665x_audio_set_operationMode(i2c_file_desc, OnOff? 0 : 1);//standby mode = 1
}

static void radio_modeSwitch(uint i2c_file_desc, bool mode_switch, AR_TuningAction_t mode, ushort frequency)
{

	if(mode_switch)	//FM
	{
		FM_tune_to(i2c_file_desc, mode, frequency);
	}
	else //AM
	{
		AM_tune_to(i2c_file_desc, mode, frequency);
	}
}

static int tef665x_wait_active(uint i2c_file_desc)
{
	TEF665x_STATE status;
	//usleep(50000);
	if(SET_SUCCESS == appl_get_operation_status(i2c_file_desc, &status))
	{
		AFB_INFO("got status", 1);
		if((status != eDevTEF665x_Boot_state) && (status != eDevTEF665x_Idle_state))
		{
			AFB_INFO("active status", 1);

			if(SET_SUCCESS == tef665x_para_load(i2c_file_desc))
			{
				_debug("parameters loaded", 1);
			}
			else
			{
				_debug("parameters not loaded", 0);
				return 0;
			}

			if(current_band == BAND_FM){
				FM_tune_to(i2c_file_desc, eAR_TuningAction_Preset, current_fm_frequency / 10000);// tune to min
			} else {
				AM_tune_to(i2c_file_desc, eAR_TuningAction_Preset, current_am_frequency / 1000);// tune to min
			}

			if(SET_SUCCESS == audio_set_mute(i2c_file_desc, 1))//unmute=0
			{
				_debug("muted", 1);
			}
			else
			{
				_debug("not muted", 0);
				return 0;
			}

			if(SET_SUCCESS == audio_set_volume(i2c_file_desc, 23))//set to -10db
			{
				_debug("set vol to", 25);
			}
			else
			{
				_debug("vol not set", 0);
				return 0;
			}
			return 1;
		}
	}

	return 0;
}

static void tef665x_chip_init(int i2c_file_desc)
{
	if(1 == tef665x_power_on(i2c_file_desc)) _debug("tef665x_power_on", 1);
	usleep(50000);
	if(1 == tef665x_boot_state(i2c_file_desc)) _debug("tef665x_boot_state", 1);
	usleep(100000);
	if(1 == tef665x_idle_state(i2c_file_desc)) _debug("tef665x_idle_state", 1);
	usleep(200000);
	if(1 == tef665x_wait_active(i2c_file_desc)) _debug("tef665x_wait_active", 1);
	//if you want to use analog output comment below command, or pass 1 to it.
	if(SET_SUCCESS != tef665x_audio_set_ana_out(i2c_file_desc, TEF665X_Cmd_Set_Output_signal_dac, 0))
	{
		_debug("Set DAC to OFF failed", 0);
		//return 0;
	}

	if(SET_SUCCESS != tef665x_set_output_src(i2c_file_desc, TEF665X_Cmd_Set_Output_signal_i2s,
															TEF665X_Cmd_Set_Output_source_aProcessor))
	{
		_debug("Set output failed", 0);
		//return 0;
	}
	//this is needed to use digital output
	if(SET_SUCCESS != tef665x_audio_set_dig_io(i2c_file_desc, TEF665X_AUDIO_CMD_22_SIGNAL_i2s1,
																TEF665X_AUDIO_CMD_22_MODE_voltage,
																TEF665X_AUDIO_CMD_22_FORMAT_16,
																TEF665X_AUDIO_CMD_22_OPERATION_slave,
																TEF665X_AUDIO_CMD_22_SAMPLERATE_48K))
	{
		_debug("Setup i2s failed", 0);
		//return 0;
	}


}


static int i2c_init(const char *i2c, int state, uint *i2c_file_desc)
{
    int fd, t;

	if(state == _open)
	{
		fd = open(i2c, O_RDWR);

		if(fd < 0)
		{
			_debug("could not open %s", i2c);
			return fd;
		}

		t = ioctl(fd, I2C_SLAVE, I2C_ADDRESS);
		if (t < 0)
		{
			_debug("could not set up slave ", 0);
			return t;
		}
		*i2c_file_desc = fd;
	}
	else
	{
		close(*i2c_file_desc);
	}

	return 0;
}

static void tef665x_start(void)
{
	int ret;
	 if(!present)
		return;

	_debug("file_desc ", file_desc);

	audio_set_mute(file_desc, 0);

 	if(!running) {

		// Start pipeline
		ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);
		_debug("gst_element_set_state to play", ret);
		running = true;
	}
 }

static void tef665x_stop(void)
{
	int ret;
	GstEvent *event;
	audio_set_mute(file_desc, 1);

	 if(present && running) {
		// Stop pipeline
		running = false;
		ret = gst_element_set_state(pipeline, GST_STATE_PAUSED);
		_debug("gst_element_set_state to pause", ret);

		// Flush pipeline
		// This seems required to avoidstatic  stutters on starts after a stop
		event = gst_event_new_flush_start();
		gst_element_send_event(GST_ELEMENT(pipeline), event);
		event = gst_event_new_flush_stop(TRUE);
		gst_element_send_event(GST_ELEMENT(pipeline), event);
	}
}

static int tef665x_init()
{
	char gst_pipeline_str[GST_PIPELINE_LEN];
	int rc;
	int ret = i2c_init(I2C_DEV, _open, &file_desc);

	current_am_frequency = known_am_band_plans[am_bandplan].min;
	current_fm_frequency = known_fm_band_plans[fm_bandplan].min;

	_debug("file_desc= ", file_desc);

	ret = appl_get_identification(file_desc);
	if(ret != 1){
		AFB_ERROR("no tef665x!");
		return -1;
	}

	current_band = BAND_AM;

	radio_powerSwitch(file_desc, 1);

	tef665x_chip_init(file_desc);

 	// Initialize GStreamer
	gst_init(NULL, NULL);

	// Use PipeWire output
	// This pipeline is working on imx6solo, the important thing, up to now, is that it gets xrun error every few seconds.
	// I believe it's related to wireplumber on imx6.
	rc = snprintf(gst_pipeline_str,
		      	GST_PIPELINE_LEN,
				 "alsasrc ! audioconvert ! audioresample ! audio/x-raw, rate=(int)48000, channels=(int)2 \
				 ! pwaudiosink stream-properties=\"p,media.role=Multimedia\" latency-time=(int)35000");

	if(rc >= GST_PIPELINE_LEN) {
		AFB_ERROR("pipeline string too long");
		return -1;
	}
	printf("pipeline: , %s\n", gst_pipeline_str);

	pipeline = gst_parse_launch(gst_pipeline_str, NULL);
	if(!pipeline) {
		AFB_ERROR("pipeline construction failed!");
		return -1;
	}

	// Start pipeline in paused state
	ret = gst_element_set_state(pipeline, GST_STATE_PAUSED);
	_debug("gst_element_set_state to pause (at the begining)", ret);

	ret = gst_bus_add_watch(gst_element_get_bus(pipeline), (GstBusFunc) handle_message, NULL);
	_debug("gst_bus_add_watch   ret", ret);

	present = true;

    return 0;
}

static void tef665x_set_frequency_callback(radio_freq_callback_t callback,
				      void *data)
{
	//freq_callback = callback;
	//freq_callback_data = data;
}
static void tef665x_set_output(const char *output)
{
}

static radio_band_t tef665x_get_band(void)
{
	_debug("band", current_band);
	return current_band;
}

static void tef665x_set_band(radio_band_t band)
{
	uint fd;
	int ret = i2c_init(I2C_DEV, _open, &fd);

	_debug("i2c_init ret value", ret);

	if(band == BAND_FM){
		current_band = band;
		FM_tune_to(fd, eAR_TuningAction_Preset, current_fm_frequency / 10000);
	} else {
		current_band = band;
		AM_tune_to(fd, eAR_TuningAction_Preset, current_am_frequency / 1000);
	}

	i2c_init(I2C_DEV, _close, &fd);

	_debug("band", current_band);
}

static uint32_t tef665x_get_frequency(void)
{
	if(current_band == BAND_FM){
		return current_fm_frequency;
	} else {
		return current_am_frequency;
	}
}

static void tef665x_set_frequency(uint32_t frequency)
{
	uint fd, f;
	int ret = i2c_init(I2C_DEV, _open, &fd);

	_debug("i2c_init ret value", ret);

	if(!present)
		return;

	if(scanning)
		return;

	if(current_band == BAND_FM) {
		if(frequency < known_fm_band_plans[fm_bandplan].min ||
	    	frequency > known_fm_band_plans[fm_bandplan].max ) {
			_debug("invalid FM frequency", frequency);
			return;
			}
	} else {
		if(frequency < known_am_band_plans[am_bandplan].min ||
	   		frequency > known_am_band_plans[am_bandplan].max ) {
			_debug("invalid AM frequency", frequency);
			return;
		}
	}

	//tef665x_scan_stop();

	if(current_band == BAND_FM){
		current_fm_frequency = frequency;
	   _debug("frequency set to FM :", frequency);
		FM_tune_to(fd, eAR_TuningAction_Preset, frequency / 10000);
	} else {
		current_am_frequency = frequency;
	   _debug("frequency set to AM :", frequency);
		AM_tune_to(fd, eAR_TuningAction_Preset, frequency / 1000);
	}



	//freq_callback(current_frequency, freq_callback_data);

	i2c_init(I2C_DEV, _close, &fd);
}

static int tef665x_band_supported(radio_band_t band)
{
	if(band == BAND_FM || band == BAND_AM)
		return 1;
	return 0;
}

static uint32_t tef665x_get_min_frequency(radio_band_t band)
{
	if(band == BAND_FM) {
		return known_fm_band_plans[fm_bandplan].min;
	} else {
		return known_am_band_plans[am_bandplan].min;
	}
}

static uint32_t tef665x_get_max_frequency(radio_band_t band)
{
	if(band == BAND_FM) {
		return known_fm_band_plans[fm_bandplan].max;
	} else {
		return known_am_band_plans[am_bandplan].max;
	}
}

static uint32_t tef665x_get_frequency_step(radio_band_t band)
{
	uint32_t ret = 0;

	switch (band) {
	case BAND_AM:
		ret = known_am_band_plans[am_bandplan].step;
		break;
	case BAND_FM:
		ret = known_fm_band_plans[fm_bandplan].step;
		break;
	default:
		break;
	}
	return ret;
}
radio_impl_ops_t tef665x_impl_ops = {
	.name = "TEF665x",
	.init = tef665x_init,
	.start = tef665x_start,
	.stop = tef665x_stop,
	.set_output = tef665x_set_output,
	.get_frequency = tef665x_get_frequency,
	.set_frequency = tef665x_set_frequency,
	.set_frequency_callback = tef665x_set_frequency_callback,
	.get_band = tef665x_get_band,
	.set_band = tef665x_set_band,
	.band_supported = tef665x_band_supported,
	.get_min_frequency = tef665x_get_min_frequency,
	.get_max_frequency = tef665x_get_max_frequency,
	.get_frequency_step = tef665x_get_frequency_step,
	/*.scan_start = tef665x_scan_start,
	.scan_stop = tef665x_scan_stop,
	.get_stereo_mode = tef665x_get_stereo_mode,
	.set_stereo_mode = tef665x_set_stereo_mode,
	.get_rds_info = tef665x_get_rds_info*/
};
